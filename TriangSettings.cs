﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TriangViewer
{
    public partial class TriangSettings : Form
    {
        public TriangSettings()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.TrVNodesCount = (int)numericUpDown1.Value;
            Properties.Settings.Default.TrVByX = int.Parse(textBox1.Text);
            Properties.Settings.Default.TrVByY = int.Parse(textBox2.Text);
            Close();
        }

        private void TriangSettings_Load(object sender, EventArgs e)
        {
            numericUpDown1.Value = Properties.Settings.Default.TrVNodesCount;
            textBox1.Text = Properties.Settings.Default.TrVByX.ToString();
            textBox2.Text = Properties.Settings.Default.TrVByY.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
