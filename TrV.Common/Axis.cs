﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TriangViewer.TrV.Common
{
    public class Axis : Matrix, IDrawable
    {
        private Pen _xPen = new Pen(Color.White, 2);
        private Pen _yPen = new Pen(Color.Red, 2);
        private Pen _zPen = new Pen(Color.Blue, 2);

        public Axis()
            : base(4, 3)
        {
            this[0, 0] = 100;
            this[1, 0] = 0;
            this[2, 0] = 0;
            this[3, 0] = 1;

            this[0, 1] = 0;
            this[1, 1] = 100;
            this[2, 1] = 0;
            this[3, 1] = 1;

            this[0, 2] = 0;
            this[1, 2] = 0;
            this[2, 2] = 100;
            this[3, 2] = 1;
        }

        public void Draw(System.Drawing.Graphics graphics, TriangViewer.TrV.Geometry.Point center)
        {
            float cx = (float)center.x;
            float cy = (float)center.y;
            var font = new Font("Microsoft Sans Serif", 12F);

            graphics.DrawString("x", font, Brushes.AliceBlue, cx + (float)this[0, 0], cy + (float)this[1, 0]);
            graphics.DrawString("y", font, Brushes.AliceBlue, cx + (float)this[0, 1], cy + (float)this[1, 1]);
            graphics.DrawString("z", font, Brushes.AliceBlue, cx + (float)this[0, 2], cy + (float)this[1, 2]);

            graphics.DrawLine(_xPen, cx, cy, cx + (float)this[0, 0], cy + (float)this[1, 0]);
            graphics.DrawLine(_yPen, cx, cy, cx + (float)this[0, 1], cy + (float)this[1, 1]);
            graphics.DrawLine(_zPen, cx, cy, cx + (float)this[0, 2], cy + (float)this[1, 2]);
        }

        public void Transform(Matrix matrix)
        {
            _data = (matrix * this)._data;
            Norm();
        }
    }
}
