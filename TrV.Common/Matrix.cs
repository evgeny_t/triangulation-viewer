﻿
namespace TriangViewer.TrV.Common
{
    using System;

    public class Matrix
    {
        public const double Rad = 0.0175;

        public double[,] _data;

        public static Matrix RotateOZ = new Matrix() 
        { 
            _data = new double[4, 4] {
                { Math.Cos(Rad), -Math.Sin(Rad),  0,  0 }, 
                { Math.Sin(Rad),  Math.Cos(Rad),  0,  0 }, 
                { 0,              0,              1,  0 },
                { 0,              0,              0,  1 }} 
        };

        public static Matrix RotateOX = new Matrix() 
        { 
            _data = new double[4, 4] {
                { 1,              0,              0,  0 },
                { 0,  Math.Cos(Rad), -Math.Sin(Rad),  0 }, 
                { 0,  Math.Sin(Rad),  Math.Cos(Rad),  0 }, 
                { 0,              0,              0,  1 }} 
        };

        public static Matrix RotateOY = new Matrix() 
        { 
            _data = new double[4, 4] {
                { Math.Cos(Rad),  0, Math.Sin(Rad),   0 }, 
                { 0,              1,              0,  0 },
                { -Math.Sin(Rad), 0, Math.Cos(Rad),   0 }, 
                { 0,              0,              0,  1 }} 
        };

        public static Matrix RorateOZ(double rad)
        {
            var rot = new Matrix(4, 4);
            rot._data = new double[4, 4] {
                { Math.Cos(Rad*rad), -Math.Sin(Rad*rad),  0,  0 }, 
                { Math.Sin(Rad*rad),  Math.Cos(Rad*rad),  0,  0 }, 
                { 0,              0,              1,  0 },
                { 0,              0,              0,  1 }};
            return rot;
        }

        public static Matrix RorateOY(double rad)
        {
            var rot = new Matrix(4, 4);
            rot._data = new double[4, 4] {
                { Math.Cos(Rad*rad),  0, Math.Sin(Rad*rad),   0 }, 
                { 0,              1,              0,  0 },
                { -Math.Sin(Rad*rad), 0, Math.Cos(Rad*rad),   0 }, 
                { 0,              0,              0,  1 }};
            return rot;
        }

        public static Matrix RorateOX(double rad)
        {
            var rot = new Matrix(4, 4);
            rot._data = new double[4, 4] {
                { 1,              0,              0,  0 },
                { 0,  Math.Cos(Rad*rad), -Math.Sin(Rad*rad),  0 }, 
                { 0,  Math.Sin(Rad*rad),  Math.Cos(Rad*rad),  0 }, 
                { 0,              0,              0,  1 }};
            return rot;
        }

        public double this[int i, int j]
        {
            get
            {
                return _data[i, j];
            }
            set
            {
                _data[i, j] = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Matrix))
                return false;
            return this == (Matrix)obj;
        }

        public static bool operator ==(Matrix m1, Matrix m2)
        {
            if (m1.Rows != m2.Rows || m1.Cols != m2.Cols)
            {
                return false;
            }

            for (int i = 0; i < m1.Rows; ++i)
            {
                for (int j = 0; j < m1.Cols; ++j)
                {
                    if (m1[i, j] != m2[i, j])
                        return false;
                }
            }
            return true;
        }

        public static bool operator !=(Matrix m1, Matrix m2)
        {
            return !(m1 == m2);
        }

        public int Rows
        {
            get
            {
                return _data.GetLength(0);
            }
        }

        public int Cols
        {
            get
            {
                return _data.GetLength(1);
            }
        }

        public Matrix()
        {
        }

        public Matrix(int n, int m)
        {
            _data = new double[n, m];
        }

        public Matrix(Matrix m)
        {
            _data = (double[,])m._data.Clone();
        }

        public static Matrix operator* (Matrix m1, Matrix m2)
        {
            if (m1.Cols != m2.Rows)
            {
                // TODO: exception
                throw new Exception("Wrong rank of matrixes");
            }

            var result = new Matrix(m1.Rows, m2.Cols);

            for (int i = 0; i < m1.Rows; ++i)
            {
                for (int j = 0; j < m2.Cols; ++j )
                {
                    result[i, j] = 0;
                    for (int k = 0; k < m1.Cols; ++k)
                    {
                        result[i, j] += m1[i, k] * m2[k, j];
                    }
                }
            }

            return result;
        }

        public static Matrix AlgAddition(Matrix matrix, int n, int m)
        {
            Matrix result = new Matrix(matrix.Rows - 1, matrix.Cols - 1);
            for (int i = 0; i < result.Rows; ++i )
            {
                for (int j = 0; j < result.Cols; ++j)
                {
                    result[i, j] = matrix[i + (i >= n ? 1 : 0), j + (j >= m ? 1 : 0)];
                }
            }
            return result;
        }

        public double Det()
        {
            if (Rows != Cols)
            {
                throw new Exception("Wrong size of matrix");
            }

            if (Rows == 1)
            {
                return _data[0, 0];
            }

            if (Rows == 2)
            {
                return _data[0, 0] * _data[1, 1] - _data[1, 0] * _data[0, 1];
            }

            double d = 0.0;
            for (int i = 0; i < Cols; ++i )
            {
                d += _data[0, i] * AlgAddition(this, 0, i).Det() * (i % 2 == 0 ? 1 : -1);
            }

            return d;
        }

        public void Norm()
        {
            for (int i = 0; i < Cols; ++i)
            {
                if (this[3,i] == 0)
                {
                    continue;
                }

                for (int j = 0; j < 3; ++j)
                {
                    this[j, i] /= this[3, i];
                }
            }
        }
    }
}
