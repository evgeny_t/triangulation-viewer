﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

namespace TriangViewer.TrV.Common
{
    public class Polygon3D : Matrix, IDrawable
    {
        public Pen Pen
        {
            get;
            set;
        }

        public Polygon3D(Geometry.Polygon polygon2D, double[] z)
            : base(4, polygon2D.Count)
        {
            if (polygon2D.Count != z.Count())
            {
                throw new ArgumentException("Polygon3D: wrong array length");
            }

            for (int i = 0; i < polygon2D.Count; ++i)
            {
                this[0, i] = polygon2D[i].x;
                this[1, i] = polygon2D[i].y;
                this[2, i] = z[i];
                this[3, i] = 1;
            }

            Pen = new Pen(Color.Coral, 1);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        #region IDrawable Members

        public void Draw(System.Drawing.Graphics graphics, Geometry.Point c)
        {
            for (int i = 0; i < Cols; ++i)
            {
                if (i == Cols - 1)
                {
                    graphics.DrawLine(
                        Pen,
                        (float)(c.x + base[0, i]),
                        (float)(c.y + base[1, i]),
                        (float)(c.x + base[0, 0]),
                        (float)(c.y + base[1, 0]));
                }
                else
                {
                    graphics.DrawLine(
                        Pen,
                        (float)(c.x + base[0, i]),
                        (float)(c.y + base[1, i]),
                        (float)(c.x + base[0, i + 1]),
                        (float)(c.y + base[1, i + 1]));
                }
            }

            graphics.DrawLine(
                Pen,
                (float)(c.x + base[0, 0]),
                (float)(c.y + base[1, 0]),
                (float)(c.x + base[0, 1]),
                (float)(c.y + base[1, 1]));
        }

        public void Transform(Matrix matrix)
        {
            _data = (matrix * this)._data;
            Norm();
        }

        #endregion
    }
}
