﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TriangViewer.TrV.Common
{
    public interface IDrawable
    {
        void Draw(Graphics graphics, Geometry.Point center);
        void Transform(Matrix matrix);
    }
}
