﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TriangViewer.TrV.Common
{
    public class Point : Matrix, IDrawable
    {
        public Pen Pen
        {
            get;
            set;
        }

        public Point(double x, double y, double z)
            :base(4, 1)
        {
            base[0, 0] = x;
            base[1, 0] = y;
            base[2, 0] = z;
            base[3, 0] = 1;

            Pen = new Pen(Color.Black, 1);
        }

        public Point(Matrix m)
            :base(4, 1)
        {

            base[0, 0] = m[0, 0];
            base[1, 0] = m[1, 0];
            base[2, 0] = m[2, 0];
            base[3, 0] = m[3, 0];
        }

        #region IDrawable Members

        public void Draw(Graphics graphics, Geometry.Point center)
        {
            graphics.DrawLine(
                Pen, 
                (float)(center.x + base[0, 0]),
                (float)(center.y + base[1, 0]),
                (float)(center.x + base[0, 0]),
                (float)(center.y + base[1, 0]));
        }

        public void Transform(Matrix matrix)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
