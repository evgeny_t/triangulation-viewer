﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TriangViewer.TrV.Compiler;

namespace TriangViewer.TrV.Common
{
    public class Triangulate3D
    {
        private static Random _random = new Random();

        public IComputable Function
        {
            get;
            set;
        }

        public double MinX
        {
            get;
            set;
        }

        public double MaxX
        {
            get;
            set;
        }

        public double MinY
        {
            get;
            set;
        }

        public double MaxY
        {
            get;
            set;
        }

        public Triangulate3D(IComputable function, double minx, double maxx, double miny, double maxy)
        {
            Function = function;
            MinX = minx;
            MaxX = maxx;
            MinY = miny;
            MaxY = maxy;
        }

        public Triangulate3D(IComputable function)
        {
            Function = function;
            MinX = -400;
            MaxX = 400;
            MinY = -400;
            MaxY = 400;

            //MinX = -100;
            //MaxX = 100;
            //MinY = -100;
            //MaxY = 100;
        }

        public Triangulate3D()
        {
        }

        public List<Polygon3D> Create(int nodeCount, out int elapsed)
        {
            var result = new List<Polygon3D>();
            var points = GetNodes(nodeCount, MinX, MaxX, MinY, MaxY);

            var before = DateTime.Now.Millisecond;
            var bs = DateTime.Now.Second;

            var t1 = DateTime.Now.Ticks;

            var poly = Geometry.Triangulate.Create(points.ToArray());

            var t2 = DateTime.Now.Ticks;

            var after = DateTime.Now.Millisecond;
            var afs = DateTime.Now.Second;
            elapsed = after - before + (afs-bs)*1000;
            elapsed = (int)(t2 - t1)/10000;

            for (var i = 0; i < poly.Count; ++i)
            {
                var z = new double[poly[i].Count];
                for (var j = 0; j < poly[i].Count; ++j)
                {
                    z[j] = Function.Compute(new[] { poly[i][j].x, poly[i][j].y });
                }
                result.Add(new Polygon3D(poly[i], z));
            }

            return result;
        }

        public static List<Geometry.Point> GetNodes(int nodeCount, double minX, double maxX, double minY, double maxY)
        {
            if (nodeCount == 0)
            {
                return new List<Geometry.Point>();
            }

            if (nodeCount < 4)
            {
                var res = new List<Geometry.Point>();
                for (var i=0; i<nodeCount; ++i)
                {
                    res.Add(new Geometry.Point(RandomFromRange(minX, maxX), RandomFromRange(minY, maxY)));
                }

                return res;
            }

            var points = new List<Geometry.Point>();

            var c = nodeCount / 4;

            var p = GetNodes(c, minX, (minX + maxX) / 2, minY, (minY + maxY) / 2);
            points.AddRange(p);

            p = GetNodes(c, minX, (minX + maxX) / 2, (minY + maxY) / 2, maxY);
            points.AddRange(p);

            p = GetNodes(c, (minX + maxX) / 2, maxX, minY, (minY + maxY) / 2);
            points.AddRange(p);

            p = GetNodes(nodeCount - 3*c, (minX + maxX) / 2, maxX, (minY + maxY) / 2, maxY);
            points.AddRange(p);

            return points;
        }

        private static double RandomFromRange(double min, double max)
        {
            double d = max - min;
            double result = (double)_random.Next((int)d) / d;
            result = min + result * d;
            return result;
        }
    }
}
