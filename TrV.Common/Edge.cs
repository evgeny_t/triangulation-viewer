﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TriangViewer.TrV.Common
{
    public class Edge : Matrix, IDrawable
    {
        public Pen Pen
        {
            get;
            set;
        }

        public Edge(double x1, double y1, double z1, double x2, double y2, double z2)
            :base(4, 2)
        {
            base[0, 0] = x1;
            base[1, 0] = y1;
            base[2, 0] = z1;
            base[3, 0] = 1;

            base[0, 1] = x2;
            base[1, 1] = y2;
            base[2, 1] = z2;
            base[3, 1] = 1;

            Pen = new Pen(Color.Coral, 1);
        }

        public Edge(Matrix m)
            :base(4, 2)
        {
            base[0, 0] = m[0, 0];
            base[1, 0] = m[1, 0];
            base[2, 0] = m[2, 0];
            base[3, 0] = m[3, 0];

            base[0, 1] = m[0, 1];
            base[1, 1] = m[1, 1];
            base[2, 1] = m[2, 1];
            base[3, 1] = m[3, 1];
        }

        #region IDrawable Members

        public void Draw(Graphics graphics, Geometry.Point center)
        {
            graphics.DrawLine(
                Pen, 
                (float)(center.x + base[0, 0]),
                (float)(center.y + base[1, 0]),
                (float)(center.x + base[0, 1]),
                (float)(center.y + base[1, 1]));
        }

        public void Transform(Matrix matrix)
        {
            _data = (matrix * this)._data;
            Norm();
        }

        #endregion
    }
}
