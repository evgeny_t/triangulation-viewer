﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TriangViewer.TrV.Common
{
    public class Scene : IDrawable
    {
        private List<IDrawable> _drawings;

        public Scene()
        {
            _drawings = new List<IDrawable>();
        }

        public void Draw(Graphics graphics, Geometry.Point center)
        {
            _drawings.ForEach(x => x.Draw(graphics, center));
        }

        public void Transform(Matrix matrix)
        {
            _drawings.ForEach(x => x.Transform(matrix));
        }
        

        public void AddDrawing(IDrawable drawing)
        {
            _drawings.Add(drawing);
        }

        public void Clear()
        {
            _drawings.Clear();
        }
    }
}
