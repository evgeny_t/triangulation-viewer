﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Windows.Forms;

using Matrix = TriangViewer.TrV.Common.Matrix;
using TriangViewer.TrV.Compiler;

namespace TriangViewer
{
    public partial class MainForm : Form
    {
        private List<IComputable> _functions = new List<IComputable>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            viewer1.Draw();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            InitMenu();
        }

        private void InitMenu()
        {
            var cd = Environment.CurrentDirectory;
            var dlls = Directory.GetFiles(cd, "*.dll");
            foreach (var dll in dlls)
                _functions.AddRange(LoadAssembly(dll));

            if (menuStrip1.Items[1] is ToolStripMenuItem)
            {
                foreach (var func in _functions)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem(func.ToString());
                    item.CheckOnClick = true;
                    item.CheckStateChanged += new EventHandler(item_CheckStateChanged);
                    item.Click += new EventHandler(item_Click);
                    ((ToolStripMenuItem)menuStrip1.Items[1]).DropDownItems.Add(item);
                }
            }
        }

        void item_Click(object sender, EventArgs e)
        {
            sender.ToString();
            // TODO:
        }

        void item_CheckStateChanged(object sender, EventArgs e)
        {
            if (!((ToolStripMenuItem)sender).Checked)
                return;
            var items = ((ToolStripMenuItem)menuStrip1.Items[1]).DropDownItems;

            foreach (ToolStripMenuItem item in items)
            {
                if (item.ToString() != sender.ToString())
                    item.Checked = false;
            }
            
            sender.ToString();
        }

        private List<IComputable> LoadAssembly(string assemblyName)
        {
            List<IComputable> func = new List<IComputable>();
            Assembly assembly = Assembly.LoadFrom(assemblyName);
            Type[] types = assembly.GetTypes();

            foreach (Type t in types)
            {
                try
                {
                    var ca = t.GetCustomAttributes(typeof(FunctionAttribute), true);
                    if (ca != null)
                    {
                        var obj = Activator.CreateInstance(t);
                        func.Add(obj as IComputable);
                    }
                }
                catch
                {
	                // TODO:
                }
            }

            return func;
        }

        private void viewer1_Load(object sender, EventArgs e)
        {

        }

        private void viewer1_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private IComputable SelectedFunction()
        {
            ToolStripMenuItem checkedItem = null;
            var items = ((ToolStripMenuItem)menuStrip1.Items[1]).DropDownItems;

            foreach (ToolStripMenuItem item in items)
            {
                if (item.Checked)
                {
                    checkedItem = item;
                    break;
                }
            }

            if (checkedItem == null)
                return null;

            IComputable function = null;

            foreach (var func in _functions)
            {
                if (func.ToString() == checkedItem.ToString())
                {
                    function = func;
                    break;
                }
            }

            return function;
        }

        private void функцииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (menuStrip1.Items[1] is ToolStripMenuItem)
            {
                var function = SelectedFunction();

                if (function == null)
                    return;

                FunctionSettings dlg = new FunctionSettings(function);
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    foreach (var p in dlg.Params)
                    {
                        if (function.Params.ContainsKey(p.Key))
                            function.Params[p.Key] = p.Value;
                    }
                }
            }
        }

        private void viewer1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void триангуляцииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TriangSettings dlg = new TriangSettings();
            dlg.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var func = SelectedFunction();
            if (func == null)
            {
                MessageBox.Show("Необходимо выбрать функцию", "");
                return;
            }
            viewer1.Scene.Clear();

            string text = string.Empty;
            text = string.Format("Функция: {0}", func.ToString());

            foreach (var p in func.Params)
            {
                text = string.Format("{0}\nПараметр {1} = {2}", text, p.Key, p.Value);
            }

            var nodesCount = Properties.Settings.Default.TrVNodesCount;

            text = string.Format("{0}\nУзлов триангуляции {1}", text, nodesCount);

            viewer1.Text = text;

            var t = new TrV.Common.Triangulate3D(func);

            var x = Properties.Settings.Default.TrVByX;
            var y = Properties.Settings.Default.TrVByY;
            t.MaxX = x / 2;
            t.MinX = -x / 2;
            t.MaxY = y / 2;
            t.MinY = -y / 2;

            int elapsed;
            var list = t.Create(nodesCount, out elapsed);

            viewer1.Text += string.Format("\nВремя построения триангуляции: {0}", elapsed / 1000.0);

            foreach (var p in list)
            {
                viewer1.Scene.AddDrawing(p);
            }

            viewer1.Scene.AddDrawing(new TrV.Common.Axis());

            for (int i = 0; i < 45; ++i) viewer1.Scene.Transform(Matrix.RotateOX);

            timer1.Interval = 10;
            timer1.Enabled = true;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.Save();
        }
    }
}
