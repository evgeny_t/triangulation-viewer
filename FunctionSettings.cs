﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TriangViewer.TrV.Compiler;

namespace TriangViewer
{
    public partial class FunctionSettings : Form
    {
        public Dictionary<string, double> Params { get; set; }

        public FunctionSettings(IComputable function)
        {
            InitializeComponent();
            Params = new Dictionary<string, double>();

            var ctop = 10;
            foreach (var i in function.Params)
            {
                var item = new FuncSettingsItem(i.Key);
                item.Top = ctop;
                ctop += item.Height + 5;
                Height = (Height < ctop + item.Height) ? ctop + item.Height : Height;
                groupBox1.Height = (groupBox1.Height < ctop + item.Height) ? ctop + item.Height : groupBox1.Height;
                groupBox1.Controls.Add(item);
            }
        }

        private void FunctionSettings_Load(object sender, EventArgs e)
        {

        }

        private void FunctionSettings_Closed(object sender, EventArgs e)
        {
            foreach (var item in groupBox1.Controls)
            {
                if (item is FuncSettingsItem)
                {
                    Params[(item as FuncSettingsItem).ParamName] = (item as FuncSettingsItem).ParamValue;
                }
                
                //item.ParamName
                //    item.ParamValue
            }
        }
    }
}
