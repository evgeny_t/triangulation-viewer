﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TriangViewer;
using TriangViewer.TrV.Compiler;

namespace CommonFunctions
{
    [Function()]
    public class Test : IComputable
    {
        public Test()
        {
            Params = new Dictionary<string, double>();
            Params["a"] = 0.0;
            Params["b"] = 0.0;
        }

        public double Compute(double[] args)
        {
            return Params["a"] * args[0] + Params["b"] * args[1];
        }

        public Dictionary<string, double> Params
        {
            get;
            set;
        }

        public override string ToString()
        {
            return "ax+by";
        }
    }

    [Function()]
    public class Test1 : IComputable
    {
        public Test1()
        {
            Params = new Dictionary<string, double>();
            Params["a"] = 0.0;
        }

        public double Compute(double[] args)
        {
            return args[0]*args[1]/Params["a"];
        }

        public Dictionary<string, double> Params
        {
            get;
            set;
        }

        public override string ToString()
        {
            return "x*y/a";
        }
    }

    [Function()]
    public class Sinx_Cosy : IComputable
    {
        public Sinx_Cosy()
        {
            Params = new Dictionary<string, double>();
            Params["a"] = 0.0;
            Params["b"] = 0.0;
            Params["c"] = 0.0;
            Params["d"] = 0.0;
        }

        public double Compute(double[] args)
        {
            return Params["a"]*Math.Sin(Params["b"]*args[0]) + Params["c"]*Math.Cos(Params["d"]*args[1]);
        }

        public Dictionary<string, double> Params
        {
            get;
            set;
        }

        public override string ToString()
        {
            return "a*sin(b*x) + c*cos(d*y)";
        }
    }
}
