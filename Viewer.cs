﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matrix = TriangViewer.TrV.Common.Matrix;

namespace TriangViewer
{
    public partial class Viewer : UserControl
    {
        private bool _isDragging = false;
        private Graphics _graphics;
        private BufferedGraphics _buffer;
        private Font _font = new Font("Microsoft Sans Serif", 8F);

        private Point _prev;

        private TrV.Common.Scene _scene;

        public TrV.Common.Scene Scene
        {
            get { return _scene; }
            set { _scene = value; }
        }

        public Viewer()
        {
            InitializeComponent();
        }

        private void Viewer_MouseDown(object sender, MouseEventArgs e)
        {
            _isDragging = true;
            _prev = e.Location;
        }

        private void Viewer_Load(object sender, EventArgs e)
        {
            _scene = new TriangViewer.TrV.Common.Scene();
            _graphics = CreateGraphics();
            _buffer = BufferedGraphicsManager.Current
                .Allocate(_graphics, new Rectangle(0, 0, Width, Height));

            _buffer.Graphics.Clear(Color.Black);
        }

        private void Viewer_Resize(object sender, EventArgs e)
        {
            _graphics = CreateGraphics();
            if (_buffer != null)
            {
                _buffer.Dispose();
            }
            _buffer = BufferedGraphicsManager.Current.Allocate(_graphics, new Rectangle(0, 0, Width, Height));
        }

        private void Viewer_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        public void Clear()
        {
            _scene.Clear();
        }

        public void Draw()
        {
            if (_scene == null)
                return;

            _buffer.Graphics.Clear(Color.Black);
            _scene.Draw(_buffer.Graphics, new TriangViewer.TrV.Geometry.Point(Width / 2, Height / 2));

            _buffer.Graphics.DrawString(Text, _font, Brushes.Cyan, 10, 30);

            _buffer.Render(_graphics);
        }

        private void Viewer_MouseUp(object sender, MouseEventArgs e)
        {
            _isDragging = false;
        }

        private void Viewer_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isDragging)
            {
                var dx = _prev.X - e.X;
                var dy = _prev.Y - e.Y;

                if (dx > 0)
                    _scene.Transform(Matrix.RorateOY(-40*0.017));
                else if (dx < 0)
                    _scene.Transform(Matrix.RorateOY(40 * 0.017));

                if (dy < 0)
                    _scene.Transform(Matrix.RorateOX(-40 * 0.017));
                else if (dy > 0)
                    _scene.Transform(Matrix.RorateOX(40 * 0.017));

                _prev = e.Location;
            }
        }
    }
}
