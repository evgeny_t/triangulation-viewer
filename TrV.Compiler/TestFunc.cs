﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TriangViewer.TrV.Compiler
{
    public class TestFunc : IComputable
    {
        #region IComputable Members

        public double Compute(double[] args)
        {
            return args[0] * args[1]/500;
            return args[0] * Math.Exp(args[1]/500);
            //var res = (args[0] * args[0] - args[1] * args[1]) * 0.01;
            
            //return args[0] * args[0] - args[1] * args[0];
        }

        public Dictionary<string, double> Params
        {
            get;
            set;
        }

        #endregion
    }
}
