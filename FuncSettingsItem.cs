﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TriangViewer.TrV.Compiler;
using System.Globalization;
using System.Threading;

namespace TriangViewer
{
    public partial class FuncSettingsItem : UserControl
    {
        private Regex regex = new Regex(@"^[-+]?[0-9]+(\.?[0-9]+)?", RegexOptions.Compiled);
        public string ParamName { get; set; }
        public double ParamValue { get; set; }

        public FuncSettingsItem(string pname)
        {
            ParamName = pname;
            InitializeComponent();

            label1.Text = pname;

            textBox1_TextChanged(null, new EventArgs());
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var valid = regex.Match(textBox1.Text).Value.Length == textBox1.Text.Length && (textBox1.Text.Length != 0);
            if (valid)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                label1.ForeColor = Color.Black;
                ParamValue = double.Parse(textBox1.Text, System.Globalization.NumberStyles.Float);
            }
            else
                label1.ForeColor = Color.Red;
            
        }
    }
}
